TARGET_BASE_NAME := $(shell cat BASENAME)

TARGET_PRESENTATION = $(TARGET_BASE_NAME)-presentation
TARGET_HANDOUT = $(TARGET_BASE_NAME)-handout
TARGET_HANDOUT2 = $(TARGET_BASE_NAME)-handout2
TARGET_PAPER = $(TARGET_BASE_NAME)-paper
TARGET_BASE = $(TARGET_BASE_NAME)-base

TARGET = $(TARGET_BASE_NAME)

DIAIMAGES     = design-ctdb-three-nodes.dia ctdb-design-daemons.dia samba-layers.dia samba-release-stream.dia

DIAIMAGES_PNG = design-ctdb-three-nodes.png ctdb-design-daemons.png samba-layers.png samba-release-stream.png

DIAIMAGES_FIG = design-ctdb-three-nodes.fig ctdb-design-daemons.fig samba-layers.fig

DIAIMAGES_SVG = design-ctdb-three-nodes.svg ctdb-design-daemons.svg samba-layers.svg

#IMAGES = $(DIAIMAGES_PNG) \
#	 regedit.png \
#	 ctdb-status.png \
#	 ctdb-status-1.png \
#	 ctdb-ip.png \
#	 ctdb-ip-1.png \
#	 smbstatus.png

IMAGES = $(DIAIMAGES_PNG)

COMMON_DEPS = base-wiki.tex base.tex $(IMAGE)

.SUFFIXES: .tex .pdf .dia .png .fig .svg .wiki

.PHONY: all

all: $(TARGET_PRESENTATION) $(TARGET_HANDOUT) $(TARGET_HANDOUT2)

.PHONY: presentation paper handout handout2

presentation: $(TARGET_PRESENTATION)

paper: $(TARGET_PAPER)

handout: $(TARGET_HANDOUT)

handout2: $(TARGET_HANDOUT2)

.PHONY: $(TARGET_PAPER) $(TARGET_PRESENTATION) $(TARGET_HANDOUT) $(TARGET_HANDOUT2)


$(TARGET_PRESENTATION): $(TARGET_PRESENTATION).pdf

$(TARGET_PRESENTATION).pdf: $(IMAGES) presentation.pdf
	cp presentation.pdf $@

presentation.pdf: presentation.tex $(COMMON_DEPS)


$(TARGET_PAPER): $(TARGET_PAPER).pdf

$(TARGET_PAPER).pdf: paper.pdf
	cp paper.pdf $@

paper.pdf: paper.tex $(COMMON_DEPS)


$(TARGET_HANDOUT): $(IMAGES) $(TARGET_HANDOUT).pdf

$(TARGET_HANDOUT).pdf: handout.pdf
	cp handout.pdf $@

handout.pdf: handout.tex $(COMMON_DEPS)


$(TARGET_HANDOUT2): $(TARGET_HANDOUT2).pdf

$(TARGET_HANDOUT2).pdf: $(IMAGES) handout2.pdf
	cp handout2.pdf $@

handout2.pdf: handout2.tex $(COMMON_DEPS)


base.tex: base-wiki.tex

base-wiki.tex: base-wiki.wiki


.wiki.tex:
	wiki2beamer $< > $@

.tex.pdf:
	pdflatex $<
	pdflatex $<

.dia.png:
	@dia -e $@ $<

.dia.fig:
	@dia -e $@ $<

.dia.svg:
	@dia -e $@ $<


.PHONY: png fig svg images

png: $(DIAIMAGES_PNG)

fig: $(DIAIMAGES_FIG)

svg: $(DIAIMAGES_SVG)

images: $(IMAGES)


.PHONY: archive

archive: $(TARGET).tar.gz

$(TARGET).tar.gz: $(TARGET).tar
	@echo "Creating $@"
	@rm -f $(TARGET).tar.gz
	@gzip $(TARGET).tar


# make $(TARGET).tar phony - it vanishes by gzipping...
.PHONY: $(TARGET).tar

$(TARGET).tar: presentation handout handout2
	@echo "Creating $@"
	@git archive --prefix=$(TARGET)/ HEAD > $@
	@rm -rf $(TARGET)
	@mkdir $(TARGET)
	@cp $(TARGET_PRESENTATION).pdf $(TARGET)
	#@cp $(TARGET_PAPER).pdf $(TARGET)
	@cp $(TARGET_HANDOUT).pdf $(TARGET)
	@cp $(TARGET_HANDOUT2).pdf $(TARGET)
	@tar rf $@ $(TARGET)/$(TARGET_PRESENTATION).pdf
	#@tar rf $@ $(TARGET)/$(TARGET_PAPER).pdf
	@tar rf $@ $(TARGET)/$(TARGET_HANDOUT).pdf
	@tar rf $@ $(TARGET)/$(TARGET_HANDOUT2).pdf


.PHONY: clean

clean:
	@git clean -f
